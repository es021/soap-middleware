echo "change dir"
cd com/jpn/soap
pwd
echo "-----"

echo "compiling java code"
javac -cp ../../../lib/json-simple-1.1.jar SoapMiddleware.java
echo "-----"

echo "change dir"
cd ../../../
pwd 
echo "-----"

echo "create new jar"
jar -cvf jpn-soap.jar com
echo "-----"

echo "move jar to lib"
mv jpn-soap.jar ./lib/jpn-soap.jar
echo "---Done----"
