 
package com.jpn.soap;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SoapMiddleware {
 public SoapMiddleware(){
     //System.out.println("Initialize Soap Middlware");
 } 

 public void interateJson(String myNamespace, JSONObject obj, SOAPBody soapBody, SOAPElement soapElem) throws SOAPException {
        JSONObject curObj = obj;

        for(Iterator iterator = obj.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            Object value = obj.get(key);
            //System.out.println(key + " | " + value.toString());
            
            SOAPElement soapElemNew = null;
            if(soapBody != null){
                soapElemNew = soapBody.addChildElement(key, myNamespace);
            }else if(soapElem != null){
                soapElemNew = soapElem.addChildElement(key);
            }
    
            if(value instanceof JSONObject){
                this.interateJson(myNamespace, (JSONObject) value, null, soapElemNew);
            }else{
                soapElemNew.setValue(value.toString());
            }
        }
    }

    public void createSoapEnvelope(SOAPMessage soapMessage, String myNamespace, String myNamespaceURI, JSONObject jsonParam) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        //System.out.println("Json Param:");
        //System.out.println(jsonParam.toString());
        //System.out.println("\n");
        //System.out.println("----");

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        
        this.interateJson(myNamespace, jsonParam, soapBody, null);
        
        //SOAPElement soapBodyElem = soapBody.addChildElement("Ref007DocumentType", myNamespace);
        //SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("USCity", myNamespace);
        //soapBodyElem1.setValue("Wan Zulsarhan");
    }

    public SOAPMessage createSOAPRequest(String soapAction, String myNamespace, String myNamespaceURI, JSONObject jsonParam) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        this.createSoapEnvelope(soapMessage, myNamespace, myNamespaceURI, jsonParam);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:--------------------------------");
        System.out.println("\n");
        soapMessage.writeTo(System.out);
        System.out.println("\n");
        System.out.println("--------------------------------");

        return soapMessage;
    }

    public String trimAllWhitespace(String xmlString){
        Pattern ptn = Pattern.compile("\\s+");
        Matcher mtch = ptn.matcher(xmlString);
        String toRet =  mtch.replaceAll(" ");

        // cannot put this will cause error parsing
        // Pattern ptn2 = Pattern.compile(" </");
        // Matcher mtch2 = ptn2.matcher(toRet);
        // toRet =  mtch2.replaceAll("</");
        return toRet;
    }

    public String callSoapWebService(String soapAction, String myNamespace, String myNamespaceURI, JSONObject jsonParam) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapRequest = this.createSOAPRequest(soapAction, myNamespace, myNamespaceURI, jsonParam);
            System.out.println("Initiate Call To => "+ soapAction);
            // System.out.println("-----------------------soapRequest------------------");
            // System.out.println(soapRequest);
            // System.out.println("-----------------------soapRequest------------------");
            // System.out.println("callSoapWebService 1");
            SOAPMessage soapResponse = soapConnection.call(soapRequest, soapAction);
            //System.out.println("Call ENDED");

            // Print the SOAP Response
            //System.out.println("Response SOAP Message:");
            //soapResponse.writeTo(System.out);
            //System.out.println();

            // to string
            //System.out.println("callSoapWebService 2");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String strMsg = new String(out.toByteArray());
            soapConnection.close();
            //System.out.println("callSoapWebService 3");


            return this.trimAllWhitespace(strMsg);
          
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
            return "Error" + e.toString();
        }
    }

    public Boolean isStrNull(String str){
        if(str != null && !str.isEmpty()) {
            return false;
        }

        return true;
    }

    public JSONObject getJsonCall(String method, String param){
        JSONObject jsonParam = new JSONObject();
        try{
            JSONParser parser = new JSONParser();
            jsonParam = (JSONObject) parser.parse(param);
        } catch(Exception e){
            e.printStackTrace();
        }

        // debug start
        //JSONObject rc = new JSONObject();
        //rc.put("ReasonCode","Waida");
        //jsonParam.put("InAsyncRequest",rc);
        // debug end

        JSONObject jsonImport = new JSONObject();
        jsonImport.put(method + "Import", jsonParam);

        JSONObject jsonCall = new JSONObject();
        jsonCall.put(method + "call", jsonImport);
        return jsonCall;
    }

     public JSONObject getJsonCallForCrs(String method, String param){
        JSONObject jsonParam = new JSONObject();
        try{
            JSONParser parser = new JSONParser();
            jsonParam = (JSONObject) parser.parse(param);
        } catch(Exception e){
            e.printStackTrace();
        }

        JSONObject jsonCall = new JSONObject();
        jsonCall.put(method + "Req", jsonParam);
        return jsonCall;
    }
}