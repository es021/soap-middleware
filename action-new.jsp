<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="java.io.*" %>
<%@ page import="java.net.*" %>
<%@ include file="_global.jsp" %>


<%
/*

POST http://10.23.178.46:9080/JOURNAL2/IJOURNAL2_INQ HTTP/1.1
Accept-Encoding: gzip,deflate
Content-Type: text/xml;charset=UTF-8
SOAPAction: "http://tempuri.org/Ijournal2Inqcall/"
Content-Length: 412
Host: 10.23.178.46:9080
Proxy-Connection: Keep-Alive
User-Agent: Apache-HttpClient/4.1.1 (java 1.5)


*/
String url = "http://10.23.178.46:9080/JOURNAL2/IJOURNAL2_INQ";
URL obj = new URL(url);
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
con.setRequestProperty("Content-Type","text/xml; charset=UTF-8");

String METHOD = "Ijournal2Inq";
String PARAM_STRING = "<InWsInputJournalSkc>" +
                "<NoPermohonan>1207101220200908104</NoPermohonan>" +
            "</InWsInputJournalSkc>";

String xml = "" +
"<SOAP-ENV:Envelope " +
"xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' " +
"xmlns:wzs='http://tempuri.org/" + METHOD + "/'>" +
"<SOAP-ENV:Header/>" +
    "<SOAP-ENV:Body>" +
        "<wzs:" + METHOD + "call>" +
        "<" + METHOD + "Import>" + PARAM_STRING + "</" + METHOD + "Import>" +
        "</wzs:" + METHOD + "call>" +
    "</SOAP-ENV:Body>" +
"</SOAP-ENV:Envelope>";

con.setDoOutput(true);
DataOutputStream wr = new DataOutputStream(con.getOutputStream());
wr.writeBytes(xml);
wr.flush();
wr.close();
String responseStatus = con.getResponseMessage();
//System.out.println("responseStatus" + responseStatus);
BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer rs = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
rs.append(inputLine);
}
in.close();
//System.out.println("response:" + rs.toString());
%>

<%= rs.toString() %>