<%@ include file="_global.jsp" %>

<%
    response.addHeader("Access-Control-Allow-Origin","*");
    
    String filename = request.getParameter("filename");
    String param = request.getParameter("param");
    String  res = "";
    if(filename == null || param == null){
        res = "One of the parameters is null (filename, param)";
    }else{
        String urlParam = param;
        String url = url_WAS() + "was-jsp/" + filename ;
        //System.out.println("url "+ url);
        //System.out.println("urlParam "+ urlParam);
        res = post(url, urlParam, FORM_URL_ENCODED);
    }
%>
<%= res %>