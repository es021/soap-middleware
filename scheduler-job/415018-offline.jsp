<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="org.json.simple.JSONArray" %>
<%@ page import="org.json.simple.parser.JSONParser" %>
<%@ page import="com.jpn.soap.SoapMiddleware" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.io.*" %>
<%@ page import="org.json.XML" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ include file="../_global.jsp" %>

<%
// #############################################################################
// GLOBAL VARIABLE  
%>
<%! public String ErrInquiryEmpty = "ErrInquiryEmpty"; %>
<%! public String ErrTxnSoapNull = "ErrTxnSoapNull"; %>
<%! public String NoError = "0"; %>
<%! public SoapMiddleware soapMiddleware = new SoapMiddleware(); %>
<%! public String AxisServiceRoot = HOST_URL + "/axis/services/"; %>
<%! public String WasUrl = ""; %>

<%! public Boolean IS_RUN_EXEC_WAS = false; %>
<%! public Boolean IS_RUN_MAIN = false; %>
<%! public Boolean IS_RUN_UPDATE_JOURNAL_STATUS = false; %>
<%!
    final void X(String x){
        if(!IS_PROD){
            System.out.println("[X] " + x); 
        }
    }

    final String getTodayDate(){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat time_formatter = new SimpleDateFormat(pattern);
        String toRet = time_formatter.format(System.currentTimeMillis());
        return toRet;
    }

    final String appendLog(String fileName, String content){
        if(fileName == null || fileName.equals("")){
            return "File Name Empty";
        }

        if(content == null || content.equals("")){
            return "Content Empty";
        }

        // get log file path
        //C:\JBOSS\jboss-5.1.0.GA\bin
        String BIN_PATH = new File("").getAbsolutePath();
        String filePath = BIN_PATH.replace("bin", "");
        filePath += "server\\default\\deploy\\scheduler.war\\log\\" + fileName + ".log";
        
        try{
            Writer fileWriter = new FileWriter(filePath, true);
            fileWriter.write(content);
            fileWriter.write(System.getProperty("line.separator"));
            fileWriter.close();
            return "Successfully created "+ filePath + "<br><br>";
        }catch(IOException e){
            return "Failed to create "+ filePath + "<br><br>" + e.getMessage();
        }
    }
%>
<%
    
    WasUrl = url_WAS();
    
    IS_RUN_EXEC_WAS = true;
    IS_RUN_MAIN = true;
    IS_RUN_UPDATE_JOURNAL_STATUS = true;

    //X("IS_RUN_EXEC_WAS " + IS_RUN_EXEC_WAS);
    //X("IS_RUN_MAIN " + IS_RUN_MAIN);
    //X("IS_RUN_UPDATE_JOURNAL_STATUS " + IS_RUN_UPDATE_JOURNAL_STATUS);
%>
<%!
// #############################################################################
// HELPER FUNCTION

    final JSONObject getTxnSoap(){
        JSONObject p = new JSONObject();
        JSONObject _p = new JSONObject();
        
        _p = new JSONObject();
        _p.put("ws", "JOURNAL/IJOURNAL_CREATE");
        _p.put("method", "IjournalCreate");
        p.put("341050",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SKC14501/SKC3414501_PERAKUAN_PERKAHWINAN");
        _p.put("method", "Skc3414501PerakuanPerkahwinan");
        p.put("341450",_p);
  
        _p = new JSONObject();
        _p.put("ws", "U3850101/U3850101_PENERIMAAN_PERMOHONAN");
        _p.put("method", "U3850101PenerimaanPermohonan");
        p.put("385010",_p);
  
        _p = new JSONObject();
        _p.put("ws", "U3850122/U3850121_PROCESS_UPDATE");
        _p.put("method", "U3850121ProcessUpdate");
        p.put("385012",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SAA50361/SAA385036");
        _p.put("method", "Saa385036");
        p.put("385036",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SAA50421/SAA385042");
        _p.put("method", "Saa385042");
        p.put("385042",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SAA50461/SAA3850461_MOHON_ANGKAT_SEMULA");
        _p.put("method", "Saa3850461MohonAngkatSemula");
        p.put("385046",_p);
  
        return p;
    }
    

    final JSONObject getJsonCallLocal(String method, String param){
        JSONObject jsonParam = new JSONObject();
        try{
            JSONParser parser = new JSONParser();
            jsonParam = (JSONObject) parser.parse(param);
        } catch(Exception e){
            e.printStackTrace();
        }

        JSONObject toRet = new JSONObject();
        toRet.put(method, jsonParam);
        return toRet;
    }

    final org.json.JSONObject getSoapResponseJboss(String webService, String method, String param){
        webService = webService.trim(); // SoapSpc5018
        method = method.trim(); // Spc415018
        param = param.trim(); //  {"InWs415018T1":{"Date":"2019-02-26"},"InWsGeneral":{"Char10":"I1"}}

        String endpointUrl = AxisServiceRoot + webService;
        JSONObject jsonParam = getJsonCallLocal(method, param);
        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + method + "/";
        String myNamespace = "jpn";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);
        
        org.json.JSONObject toRet = null;
        try{
            org.json.JSONObject jsonSoap = XML.toJSONObject(responseSoap);
            toRet = jsonSoap.getJSONObject("soapenv:Envelope");
            toRet = toRet.getJSONObject("soapenv:Body");
            toRet = toRet.getJSONObject("tns:" + method + "Response");
            toRet = toRet.getJSONObject("response");
        }catch(Exception e){
            e.printStackTrace();
        }
       
        return toRet;
    }

    /**
    @return error String, 
        kalau takde error, return null
    **/
    final String execWas(String webService, String method, String param){
        webService = webService.trim();
        method = method.trim();
        param = param.trim();

        String endpointUrl = WasUrl + webService;
        JSONObject jsonParam = soapMiddleware.getJsonCall(method, param);
        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + method + "/";
        String myNamespace = "wzs";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);
        
        org.json.JSONObject toRet = null;   
        try{
            org.json.JSONObject jsonSoap = XML.toJSONObject(responseSoap);
            X(jsonSoap.toString());

            toRet = jsonSoap.getJSONObject("soapenv:Envelope");
            toRet = toRet.getJSONObject("soapenv:Body");
            String keyResponse = toRet.keys().next().toString();
            toRet = toRet.getJSONObject(keyResponse);
            org.json.JSONObject response = toRet.getJSONObject(method + "Export");
            
            Integer errIndicator = 0;
            String errText = "";

            try{
                // check kalau ada error
                org.json.JSONObject error = response.getJSONObject("OutWsError");
                errIndicator = error.getInt("MessageIndicator");
                errText = error.getString("MessageText");
            } catch(Exception e){
                // takde error
            }
            
            if(errIndicator == 1){
                return errText;
            }
            return null;

        } catch(Exception e){
            e.printStackTrace();
            return e.toString();
        }
    }


    final String getLocalTransUrl(){
        String url = HOST_URL + "/home-webservice/auth/trans";
        return url;
    }

    final void updateJournalStatus(String WjrTxnOnlInd, String WjrAplNo, String WjrMachNo, String WjrTxnDt, String WjrBrchCd, String WjrTxnCode){
        JSONObject p = new JSONObject();

        JSONObject _p1 = new JSONObject();
        _p1.put("WJR_TXN_ONL_IND", WjrTxnOnlInd);
        _p1.put("WJR_APL_NO", WjrAplNo);
        _p1.put("WJR_MACH_NO", WjrMachNo);
        _p1.put("WJR_TXN_DT", WjrTxnDt);
        _p1.put("WJR_BRCH_CD", WjrBrchCd);
        _p1.put("WJR_TXN_CODE", WjrTxnCode);
        _p1.put("TxnType", "U1");

        X("------------------------------------------------------------------------");
        X("Start updateJournalStatus");
        String url = getLocalTransUrl();
        String txnType = "U1";
        String urlParam = "action=T415018&param="+_p1.toString();
        String res = post(url, urlParam, FORM_URL_ENCODED);
    }

    final void writeErrorLog(JSONObject err){
        X("------------------------------------------------------------------------");
        X("Start writeErrorLog");
        X(err.toString());
    }

    final JSONObject Main(){
        JSONObject Err = new JSONObject();  
        try{
            String url = getLocalTransUrl();

            JSONObject _p1 = new JSONObject();
            _p1.put("TxnType", "I1");
            _p1.put("Date", getTodayDate());

            String urlParam = "action=T415018&param=" + _p1.toString();
            String res = postHomeWebservice(url, urlParam);
            
            JSONObject resJson = new JSONObject();
            JSONArray resArray = new JSONArray();
            try{
                JSONParser parser = new JSONParser();
                resJson = (JSONObject) parser.parse(res);
                resArray = (JSONArray) resJson.get("data");

            } catch(Exception e){
                e.printStackTrace();
                Err.put("main", e.toString());
                return Err;
            }

            JSONObject TxnSoap = getTxnSoap();

            Iterator jsonArrayIterator = resArray.iterator();
            while(jsonArrayIterator.hasNext()) {
                String _err = NoError;

                JSONObject row = (JSONObject) jsonArrayIterator.next();
                                System.out.println(row);

                String aplNo = (String) row.get("WJR_APL_NO");
                if(aplNo.equals("")){
                    continue;
                }
                System.out.println(aplNo);

                String txnDt = row.get("WJR_TXN_DT").toString();
                String txnFld2 = row.get("WJR_TXN_FLD2").toString();
                String brchCd =  row.get("WJR_BRCH_CD").toString();
                String machNo =  row.get("WJR_MACH_NO").toString();
                String txnCode =  row.get("WJR_TXN_CODE").toString();

                String key = txnCode + "::" + 
                    aplNo + "::" + 
                    txnDt + "::" + 
                    brchCd + "::" + 
                    machNo;

                JSONObject txnSoap = (JSONObject) TxnSoap.get(txnCode); 
                if(txnSoap == null){
                    Err.put(key, ErrTxnSoapNull);
                    _err = ErrTxnSoapNull;
                }

                if(_err.equals(NoError)){
                    String update_ws = txnSoap.get("ws").toString();
                    String update_method = txnSoap.get("method").toString();
                    String update_param = txnFld2;
                    String errWas = null;
                    X("update_ws " + update_ws);
                    X("update_method " + update_method);
                    X("update_param " + update_param);

                    if(IS_RUN_EXEC_WAS){
                        errWas = execWas(update_ws, update_method, update_param);
                    }
                    if(errWas != null){
                        _err = errWas;
                    }
                }
                

                Err.put(key, _err);

                if(_err.equals(NoError)){
                    // kalau berjaya push host - 2
                    // String WjrTxnOnlInd, String WjrAplNo, String WjrMachNo, String WjrTxnDt, String WjrBrchCd, String WjrTxnCode
                    if(IS_RUN_UPDATE_JOURNAL_STATUS){
                        updateJournalStatus("2", aplNo, machNo, txnDt, brchCd, txnCode);
                    }
                }else{
                    // kalau gagal push host - 3
                    // String WjrTxnOnlInd, String WjrAplNo, String WjrMachNo, String WjrTxnDt, String WjrBrchCd, String WjrTxnCode
                    if(IS_RUN_UPDATE_JOURNAL_STATUS){
                        updateJournalStatus("3", aplNo, machNo, txnDt, brchCd, txnCode);
                    }
                }

            }
            

        }catch(Exception e){
            e.printStackTrace();
            if(e.toString().contains("OutTwjr")){
                Err.put("Main", ErrInquiryEmpty);
            }else{
                Err.put("Main", e.toString());
            }
        }

        return Err;
    }
    
    
%>
<%
// #############################################################################
// START MAIN PROGRAM
    response.addHeader("Access-Control-Allow-Origin","*");
    JSONObject Err = new JSONObject();
    if(IS_RUN_MAIN){
		// uncomment if ready to run
        Err = Main();
    }
%>
<%= Err.toString() %>