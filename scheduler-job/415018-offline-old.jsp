<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="org.json.simple.JSONArray" %>
<%@ page import="org.json.simple.parser.JSONParser" %>
<%@ page import="com.jpn.soap.SoapMiddleware" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.io.*" %>
<%@ page import="org.json.XML" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%
// #############################################################################
// GLOBAL VARIABLE  
%>
<%! public String ErrInquiryEmpty = "ErrInquiryEmpty"; %>
<%! public String ErrTxnSoapNull = "ErrTxnSoapNull"; %>
<%! public String NoError = "0"; %>
<%! public String WebService415018 = "SoapSpc5018"; %>
<%! public String Method415018 = "Spc415018"; %>
<%! public SoapMiddleware soapMiddleware = new SoapMiddleware(); %>
<%! public String AxisServiceRoot = "http://localhost:8080/axis/services/"; %>
<%! public Boolean IS_PROD = false; %>
<%! public String WasUrl = ""; %>

<%! public Boolean IS_FROM_SCHEDULER = false; %>
<%! public Boolean IS_RUN_EXEC_WAS = false; %>
<%! public Boolean IS_RUN_MAIN = false; %>
<%! public Boolean IS_RUN_UPDATE_JOURNAL_STATUS = false; %>
<%!
    final void X(String x){
        if(!IS_PROD){
            System.out.println("[X] " + x); 
        }
    }

    final String getTodayDate(){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat time_formatter = new SimpleDateFormat(pattern);
        String toRet = time_formatter.format(System.currentTimeMillis());
        return toRet;
    }

    final String appendLog(String fileName, String content){
        if(fileName == null || fileName.equals("")){
            return "File Name Empty";
        }

        if(content == null || content.equals("")){
            return "Content Empty";
        }

        // get log file path
        //C:\JBOSS\jboss-5.1.0.GA\bin
        String BIN_PATH = new File("").getAbsolutePath();
        String filePath = BIN_PATH.replace("bin", "");
        filePath += "server\\default\\deploy\\scheduler.war\\log\\" + fileName + ".log";
        
        try{
            Writer fileWriter = new FileWriter(filePath, true);
            fileWriter.write(content);
            fileWriter.write(System.getProperty("line.separator"));
            fileWriter.close();
            return "Successfully created "+ filePath + "<br><br>";
        }catch(IOException e){
            return "Failed to create "+ filePath + "<br><br>" + e.getMessage();
        }
    }
%>
<%
    
    // IS_PROD = !(request.getRequestURL().toString().contains("localhost"));
	IS_PROD = true;
    WasUrl = IS_PROD ? "http://WASIP:9080/" :  "http://10.23.196.65:9080/";
	
    
    String fromScheduler = request.getParameter("fromScheduler");
    IS_FROM_SCHEDULER = false;
    if(fromScheduler != null){
        if(fromScheduler.equals("1")){
            IS_FROM_SCHEDULER = true;
        }
    }
    
    if(IS_FROM_SCHEDULER){
        IS_RUN_EXEC_WAS = true;
        IS_RUN_MAIN = true;
        IS_RUN_UPDATE_JOURNAL_STATUS = true;
    }else{
        //IS_RUN_EXEC_WAS = true;
        //IS_RUN_MAIN = true;
        //IS_RUN_UPDATE_JOURNAL_STATUS = true;

        //X(appendLog("cdascs","test"));
        //X(getTodayDate());
    }

    X("IS_FROM_SCHEDULER " + IS_FROM_SCHEDULER);
    X("IS_RUN_EXEC_WAS " + IS_RUN_EXEC_WAS);
    X("IS_RUN_MAIN " + IS_RUN_MAIN);
    X("IS_RUN_UPDATE_JOURNAL_STATUS " + IS_RUN_UPDATE_JOURNAL_STATUS);
%>
<%!
// #############################################################################
// HELPER FUNCTION
    

    final JSONObject getJsonCallLocal(String method, String param){
        JSONObject jsonParam = new JSONObject();
        try{
            JSONParser parser = new JSONParser();
            jsonParam = (JSONObject) parser.parse(param);
        } catch(Exception e){
            e.printStackTrace();
        }

        JSONObject toRet = new JSONObject();
        toRet.put(method, jsonParam);
        return toRet;
    }

    final org.json.JSONObject getSoapResponseJboss(String webService, String method, String param){
        webService = webService.trim(); // SoapSpc5018
        method = method.trim(); // Spc415018
        param = param.trim(); //  {"InWs415018T1":{"Date":"2019-02-26"},"InWsGeneral":{"Char10":"I1"}}

        String endpointUrl = AxisServiceRoot + webService;
        JSONObject jsonParam = getJsonCallLocal(method, param);
        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + method + "/";
        String myNamespace = "jpn";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);
        
        org.json.JSONObject toRet = null;
        try{
            org.json.JSONObject jsonSoap = XML.toJSONObject(responseSoap);
            toRet = jsonSoap.getJSONObject("soapenv:Envelope");
            toRet = toRet.getJSONObject("soapenv:Body");
            toRet = toRet.getJSONObject("tns:" + method + "Response");
            toRet = toRet.getJSONObject("response");
        }catch(Exception e){
            e.printStackTrace();
        }
       
        return toRet;
    }

    /**
    @return error String, 
        kalau takde error, return null
    **/
    final String execWas(String webService, String method, String param){
        webService = webService.trim();
        method = method.trim();
        param = param.trim();

        String endpointUrl = WasUrl + webService;
        JSONObject jsonParam = soapMiddleware.getJsonCall(method, param);
        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + method + "/";
        String myNamespace = "wzs";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);
        
        org.json.JSONObject toRet = null;   
        try{
            org.json.JSONObject jsonSoap = XML.toJSONObject(responseSoap);
            X(jsonSoap.toString());

            toRet = jsonSoap.getJSONObject("soapenv:Envelope");
            toRet = toRet.getJSONObject("soapenv:Body");
            String keyResponse = toRet.keys().next().toString();
            toRet = toRet.getJSONObject(keyResponse);
            org.json.JSONObject response = toRet.getJSONObject(method + "Export");
            
            Integer errIndicator = 0;
            String errText = "";

            try{
                // check kalau ada error
                org.json.JSONObject error = response.getJSONObject("OutWsError");
                errIndicator = error.getInt("MessageIndicator");
                errText = error.getString("MessageText");
            } catch(Exception e){
                // takde error
            }
            
            if(errIndicator == 1){
                return errText;
            }
            return null;

        } catch(Exception e){
            e.printStackTrace();
            return e.toString();
        }
    }

    final JSONObject getTxnSoap(){
        JSONObject p = new JSONObject();
        JSONObject _p = new JSONObject();
        
        _p = new JSONObject();
        _p.put("ws", "JOURNAL/IJOURNAL_CREATE");
        _p.put("method", "IjournalCreate");
        p.put("341050",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SKC14501/SKC3414501_PERAKUAN_PERKAHWINAN");
        _p.put("method", "Skc3414501PerakuanPerkahwinan");
        p.put("341450",_p);
  
        _p = new JSONObject();
        _p.put("ws", "U3850101/U3850101_PENERIMAAN_PERMOHONAN");
        _p.put("method", "U3850101PenerimaanPermohonan");
        p.put("385010",_p);
  
        _p = new JSONObject();
        _p.put("ws", "U3850122/U3850121_PROCESS_UPDATE");
        _p.put("method", "U3850121ProcessUpdate");
        p.put("385012",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SAA50361/SAA385036");
        _p.put("method", "Saa385036");
        p.put("385036",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SAA50421/SAA385042");
        _p.put("method", "Saa385042");
        p.put("385042",_p);
  
        _p = new JSONObject();
        _p.put("ws", "SAA50461/SAA3850461_MOHON_ANGKAT_SEMULA");
        _p.put("method", "Saa3850461MohonAngkatSemula");
        p.put("385046",_p);
  
        return p;
    }

    final void updateJournalStatus(String WjrTxnOnlInd, String WjrAplNo, String WjrMachNo, String WjrTxnDt, String WjrBrchCd, String WjrTxnCode){
        /**
        let param = {};
        param[TWJR.InEntity] = {};
        param[TWJR.InEntity][TWJR.WjrAplNo] = jData[TWJR.WjrAplNo];
        param[TWJR.InEntity][TWJR.WjrMachNo] = jData[TWJR.WjrMachNo];
        param[TWJR.InEntity][TWJR.WjrTxnDt] = jData[TWJR.WjrTxnDt];
        param[TWJR.InEntity][TWJR.WjrBrchCd] = jData[TWJR.WjrBrchCd];
        param[TWJR.InEntity][TWJR.WjrTxnCode] = jData[TWJR.WjrTxnCode];
        param[TWJR.InEntity][TWJR.WjrTxnOnlInd] = onlInd;

        param = ApiHelper.addNewRequestType(param, "U1");
        param["InWsGeneral"] = {
            Char10: type,
        };
        **/
        JSONObject p = new JSONObject();

        JSONObject _p1 = new JSONObject();
        _p1.put("WjrTxnOnlInd", WjrTxnOnlInd);
        _p1.put("WjrAplNo", WjrAplNo);
        _p1.put("WjrMachNo", WjrMachNo);
        _p1.put("WjrTxnDt", WjrTxnDt);
        _p1.put("WjrBrchCd", WjrBrchCd);
        _p1.put("WjrTxnCode", WjrTxnCode);
        p.put("InTwjrWebJournal", _p1);

        JSONObject _p2 = new JSONObject();
        _p2.put("Char10", "U1");
        p.put("InWsGeneral", _p2);

        X("------------------------------------------------------------------------");
        X("Start updateJournalStatus");
        X(p.toString());
        getSoapResponseJboss(WebService415018, Method415018, p.toString());
    }

    final String getParamInq(){
        // {"InWs415018T1":{"Date":"2019-02-26"},"InWsGeneral":{"Char10":"I1"}}
        JSONObject _p1 = new JSONObject();
        _p1.put("Date", "2019-02-26");
        JSONObject _p2 = new JSONObject();
        _p2.put("Char10", "I1");

        JSONObject p = new JSONObject();
        p.put("InWs415018T1", _p1);
        p.put("InWsGeneral", _p2);

        return p.toString();
    }

    final void writeErrorLog(JSONObject err){
        X("------------------------------------------------------------------------");
        X("Start writeErrorLog");
        X(err.toString());
    }

    final JSONObject Main(){
        JSONObject Err = new JSONObject();  
        try{
            JSONObject TxnSoap = getTxnSoap();
            org.json.JSONObject response = null;

            // ###########################################################
            // 1. Inquiry sume rekod 0 dan 3
            response = getSoapResponseJboss(WebService415018, Method415018, getParamInq());
            X("response " + response.toString());
            response = response.getJSONObject("OutTwjr");
            
            org.json.JSONArray rowArray = response.getJSONArray("row");
            X("rowArray.length() " + rowArray.length());
            X("rowArray.length() " + rowArray.length());
            X("rowArray.length() " + rowArray.length());
            X("rowArray.length() " + rowArray.length());
           
            for(int i = 0; i < rowArray.length(); i++){
                String _err = NoError;
                org.json.JSONObject row = rowArray.getJSONObject(i);
                row = row.getJSONObject("OutListTwjrWebJournal");

                String aplNo = row.getString("WjrAplNo");
                if(aplNo.equals("")){
                    continue;
                }
                
                String txnDt = row.getString("WjrTxnDt");
                String txnFld2 = row.getString("WjrTxnFld2");
                String brchCd =  row.get("WjrBrchCd").toString();
                String machNo =  row.get("WjrMachNo").toString();
                String txnCode =  row.get("WjrTxnCode").toString();

                String key = txnCode + "::" + 
                    aplNo + "::" + 
                    txnDt + "::" + 
                    brchCd + "::" + 
                    machNo;

                JSONObject txnSoap = (JSONObject) TxnSoap.get(txnCode);
                if(txnSoap == null){
                    Err.put(key, ErrTxnSoapNull);
                    _err = ErrTxnSoapNull;
                }

                if(_err.equals(NoError)){
                    String update_ws = txnSoap.get("ws").toString();
                    String update_method = txnSoap.get("method").toString();
                    String update_param = txnFld2;
                    String errWas = null;
                    X("update_ws " + update_ws);
                    X("update_method " + update_method);
                    X("update_param " + update_param);

                    if(IS_RUN_EXEC_WAS){
                        errWas = execWas(update_ws, update_method, update_param);
                    }
                    if(errWas != null){
                        _err = errWas;
                    }
                }
                

                Err.put(key, _err);

                if(_err.equals(NoError)){
                    // kalau berjaya push host - 2
                    // String WjrTxnOnlInd, String WjrAplNo, String WjrMachNo, String WjrTxnDt, String WjrBrchCd, String WjrTxnCode
                    if(IS_RUN_UPDATE_JOURNAL_STATUS){
                        updateJournalStatus("2", aplNo, machNo, txnDt, brchCd, txnCode);
                    }
                }else{
                    // kalau gagal push host - 3
                    // String WjrTxnOnlInd, String WjrAplNo, String WjrMachNo, String WjrTxnDt, String WjrBrchCd, String WjrTxnCode
                    if(IS_RUN_UPDATE_JOURNAL_STATUS){
                        updateJournalStatus("3", aplNo, machNo, txnDt, brchCd, txnCode);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            if(e.toString().contains("OutTwjr")){
                Err.put("Main", ErrInquiryEmpty);
            }else{
                Err.put("Main", e.toString());
            }
        }

        return Err;
    }

    
%>
<%
/**
    "row": [{
        "OutListTwjrWebJournal": {
            "WjrTxnOnlInd": 0,
            "WjrMachNo": 0,
            "WjrTxnDt": "00000000000000000000",
            "WjrTxnFld1": "",
            "WjrTxnFld2": "",
            "WjrAplNo": "",
            "WjrBrchCd": "",
            "WjrTxnCode": 0
            }
        }, {
        "OutListTwjrWebJournal": {
            "WjrTxnOnlInd": 0,
            "WjrMachNo": 23,
            "WjrTxnDt": "20190402105643000000",
            "WjrTxnFld1": "{\"FID1001NOPERMOHONAN\":\"160110112019040210560723\",\"FID1007TARIKHPERMOHONAN\":\"20190402\",\"FID8193TEMPATPERMOHONAN\":\"16011011\",\"FID6195NOFAILRUJUKAN\":\"1231\",\"FID1879JENISPENGANGKATAN\":\"2\",\"FID4795NOTELEFONPEJABAT\":\"0388807836/7816/7829/7815/7821\",\"FID1587TEMPATPENDAFTARAN\":\"16011011\",\"FID1391TAHUNCARIAN1\":\"1232\",\"FID1393TAHUNCARIAN2\":\"1231\",\"FID1621NOKPTK\":\"930718115423\",\"FID1311NODOKUMENLAINP\":\"QWE\",\"FID1313JENISDOKUMENP\":\"02\",\"FID1317NEGARAPENGELUARP\":\"3115\",\"INDIKATORISOFFLINE\":\"1\",\"FID1312NODOKUMENLAIN\":\"QWE\",\"FID1314JENISDOKUMEN\":\"02\",\"FID1318NEGARAPENGELUAR\":\"3115\",\"FID1321NAMAPENUHP\":\"LAL\",\"FID1622NOKPTK\":\"930718115423\",\"FID1009NAMAK\":\"DWQL\",\"FID1705NAMABORANG\":\"1\",\"FID1267DOKUMEN1\":\"0\",\"FID1269DOKUMEN2\":\"1\",\"FID1271DOKUMEN3\":\"1\",\"FID1990DOKUMEN4\":\"0\",\"FID1275DOKUMEN5\":\"0\",\"FID1277DOKUMEN6\":\"0\",\"USER_ID\":\"EJPN002\",\"BRANCH_CODE\":\"16011011\",\"PC_ID\":\"023\",\"TRANS_CODE\":\"385036\",\"DATE_TODAY\":\"02/04/2019\",\"TIME_UPDATE\":\"10:56:43\"}",
            "WjrTxnFld2": "{\"InWs385036t1\":{\"Fid1001NoPermohonan\":\"160110112019040210560723\",\"Fid1007TarikhPermohonan\":\"20190402\",\"Fid8193TempatPermohonan\":\"16011011\",\"Fid6195NoFailRujukan\":\"1231\",\"Fid1879JenisPengangkatan\":\"2\",\"Fid4795NoTelefonPejabat\":\"0388807836/7816/7829/7815/7821\",\"Fid1587TempatPendaftaran\":\"16011011\",\"Fid1391TahunCarian1\":\"1232\",\"Fid1393TahunCarian2\":\"1231\"},\"InWs385036t2\":{\"Fid1621NoKptK\":\"930718115423\",\"Fid1311NoDokumenLainP\":\"QWE\",\"Fid1313JenisDokumenP\":\"02\",\"Fid1317NegaraPengeluarP\":\"3115\",\"indikatorIsOffline\":\"1\"},\"InWs385036t3\":{\"Fid1312NoDokumenLain\":\"QWE\",\"Fid1314JenisDokumen\":\"02\",\"Fid1318NegaraPengeluar\":\"3115\",\"Fid1321NamaPenuhP\":\"LAL\",\"Fid1622NoKptK\":\"930718115423\",\"Fid1009NamaK\":\"DWQL\"},\"InWs385036t4\":{\"Fid1705NamaBorang\":\"1\",\"Fid1267Dokumen1\":\"0\",\"Fid1269Dokumen2\":\"1\",\"Fid1271Dokumen3\":\"1\",\"Fid1990Dokumen4\":\"0\",\"Fid1275Dokumen5\":\"0\",\"Fid1277Dokumen6\":\"0\"},\"InUsers\":{\"OperId\":\"EJPN002\"},\"InBranch\":{\"BranchCode\":\"16011011\"},\"InWsJpnGeneral\":{\"Wsid\":\"023\",\"TransactionCode\":\"385036\"},\"InWsGeneral\":{\"Char10\":\"U3\"},\"InTwjrWebJournal\":{\"WjrAplNo\":\"160110112019040210560723\",\"WjrBrchCd\":\"16011011\",\"WjrOpHscno\":\"930718115423\",\"WjrTxnDt\":\"20190402105643000000\",\"WjrRvsDt\":\"20190402105643000000\",\"WjrCreDt\":\"20190402105643000000\",\"WjrMachNo\":\"023\",\"WjrOpUid\":\"EJPN002\",\"WjrOpCls\":\"8\",\"WjrOpLvl\":\"10110011\",\"WjrTxnCode\":385036,\"WjrIpAddr\":\"localhost:8081\",\"WjrTxnVer\":\"1\",\"WjrDeptId\":\"1\",\"WjrTxnMode\":\"N\",\"WjrTdfInd1\":32,\"WjrTdfInd2\":64,\"WjrTxnInd1\":192,\"WjrTxnFld1\":\"{\\\"Fid1001NoPermohonan\\\":\\\"160110112019040210560723\\\",\\\"Fid1007TarikhPermohonan\\\":\\\"20190402\\\",\\\"Fid8193TempatPermohonan\\\":\\\"16011011\\\",\\\"Fid6195NoFailRujukan\\\":\\\"1231\\\",\\\"Fid1879JenisPengangkatan\\\":\\\"2\\\",\\\"Fid4795NoTelefonPejabat\\\":\\\"0388807836/7816/7829/7815/7821\\\",\\\"Fid1587TempatPendaftaran\\\":\\\"16011011\\\",\\\"Fid1391TahunCarian1\\\":\\\"1232\\\",\\\"Fid1393TahunCarian2\\\":\\\"1231\\\",\\\"Fid1621NoKptK\\\":\\\"930718115423\\\",\\\"Fid1311NoDokumenLainP\\\":\\\"QWE\\\",\\\"Fid1313JenisDokumenP\\\":\\\"02\\\",\\\"Fid1317NegaraPengeluarP\\\":\\\"3115\\\",\\\"indikatorIsOffline\\\":\\\"1\\\",\\\"Fid1312NoDokumenLain\\\":\\\"QWE\\\",\\\"Fid1314JenisDokumen\\\":\\\"02\\\",\\\"Fid1318NegaraPengeluar\\\":\\\"3115\\\",\\\"Fid1321NamaPenuhP\\\":\\\"LAL\\\",\\\"Fid1622NoKptK\\\":\\\"930718115423\\\",\\\"Fid1009NamaK\\\":\\\"DWQL\\\",\\\"Fid1705NamaBorang\\\":\\\"1\\\",\\\"Fid1267Dokumen1\\\":\\\"0\\\",\\\"Fid1269Dokumen2\\\":\\\"1\\\",\\\"Fid1271Dokumen3\\\":\\\"1\\\",\\\"Fid1990Dokumen4\\\":\\\"0\\\",\\\"Fid1275Dokumen5\\\":\\\"0\\\",\\\"Fid1277Dokumen6\\\":\\\"0\\\",\\\"USER_ID\\\":\\\"EJPN002\\\",\\\"BRANCH_CODE\\\":\\\"16011011\\\",\\\"PC_ID\\\":\\\"023\\\",\\\"TRANS_CODE\\\":\\\"385036\\\",\\\"DATE_TODAY\\\":\\\"02/04/2019\\\",\\\"TIME_UPDATE\\\":\\\"10:56:43\\\"}\",\"WjrTxnFld2\":\"\",\"WjrTxnOnlInd\":\"1\"},\"InTwtrWebTxn\":{\"WtrTxnVer\":\"1\",\"WtrDeptId\":\"1\",\"WtrAplNo\":\"160110112019040210560723\",\"WtrBrchCd\":\"16011011\",\"WtrMachNo\":\"023\",\"WtrTxnDt\":\"20190402105643000000\",\"WtrRefDt\":\"20190402105643000000\",\"WtrCreDt\":\"20190402105643000000\",\"WtrUpdDt\":\"20190402105643000000\",\"WtrTxnCode\":\"385036\",\"WtrPymtInd\":\"0\",\"WtrConfInd\":\"0\",\"WtrInqInd\":\"1\",\"WtrCreUid\":\"EJPN002\",\"WtrUpdUid\":\"EJPN002\",\"WtrTxnOnlInd\":\"1\"}}",
            "WjrAplNo": "160110112019040210560723",
            "WjrBrchCd": 16011011,
            "WjrTxnCode": 385036
        }
    }];
*/
// #############################################################################
// START MAIN PROGRAM
    response.addHeader("Access-Control-Allow-Origin","*");
    JSONObject Err = new JSONObject();
    if(IS_RUN_MAIN){
		// uncomment if ready to run
        // Err = Main();
    }
    //execWas("JOURNAL/IJOURNAL_CREATE","IjournalCreate","{}");
    //X(getTxnSoap().toString());
%>
<%= Err.toString() %>