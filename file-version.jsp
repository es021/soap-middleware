
<%@ page import="java.io.*"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%!
final long folderSize(File directory) {
    if (directory.isFile()){
        return directory.length();
    }
    long length = 0;
    for (File file : directory.listFiles()) {
        if (file.isFile())
            length += file.length();
        else
            length += folderSize(file);
    }
    return length;
}
%>

<body>
<style>
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
        padding : 4px 7px;
    }
</style>
<h3>List Of File/Directory in Webapps (Tomcat)</h3>
<table>
<thead>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Size</th>
        <th>Last Modified</th>
    <tr>        
</thead>
<%
    response.addHeader("Access-Control-Allow-Origin","*");

    //System.out.println(request.getRequestURL());
    File jsp = new File(request.getRealPath(request.getServletPath()));
    File dir = jsp.getParentFile();
    dir = dir.getParentFile();
    File[] list = dir.listFiles();
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    for (File f : list) {
        long size = folderSize(f);
        //float s = size / 1000000;
        float s = size / 1000;
        String type = f.isFile() ? "file" : "directory";

        String filePath = f.toString();
        String[] arr = filePath.split("\\\\");
        if(arr.length <= 1){
            arr = filePath.split("/");
        }
    	
        String fileName = arr[arr.length -1];
        String link = request.getRequestURL().toString().replace("index.jsp", "") + fileName;
        String lastModified = sdf.format(f.lastModified());
        %>
        <tr>
            <td><%= fileName %></td>
            <td><%= type %></td>
            <td><%= size / 1000 %> KB</td>
            <td><%= lastModified %></td>
        </tr>
        <%
    } 
%>

</table>
</body>