<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="com.jpn.soap.SoapMiddleware" %>
<%@ include file="_global.jsp" %>

<%-- 
<%@ page import="org.apache.commons.io.FileUtils" %>
<%@ page import="java.io.File" %> --%>
<%!
    final String convertByteToHex(char[] chars) {
        try{
            StringBuilder sb = new StringBuilder();
            for (char c : chars) {
                System.out.println("[convertByteToHex] " + c);
                byte b = (byte) c;
                sb.append(String.format("%02x", b));
            }
           
            Integer num = Integer.parseInt(sb.toString());
            
            // validate if spaces
            if(num == 20){
                return "";
            }

            // return valid value here
            return num + "";
            
        } catch(Exception err){
            err.printStackTrace();
            return "";
        }
	}

	final String fixByteVal(String res, String indicator){
        try{
            if(res.contains(indicator)){     
                Integer indicatorIndex = res.indexOf(indicator);
                Integer valIndex = indicatorIndex + indicator.length();

                // smpai jumpa <
                Integer len = 0;
                for(Integer i = 0 ; i < 10 ; i++){
                    char c = res.charAt(valIndex + i);
                    if(c == '<'){
                        break;
                    } else {
                        len ++;
                    }
                }

                char[] valByte = new char[len];
                for(Integer i = 0; i < len; i++){
                    valByte[i] = res.charAt(valIndex + i);
                }

                System.out.println("=================================================");
                System.out.println("fixByteVal =======================================");
                System.out.println("indicator " + indicator);
                System.out.println("len " + len);
                System.out.println("valIndex " + valIndex);
                System.out.println("valByte " + valByte);

                String newVal = convertByteToHex(valByte);
                String newRes = res.substring(0, valIndex) + newVal + res.substring(valIndex + len);

                
            
                return newRes;
            } else{
                return res;
            }
        } catch(Exception err){
            err.printStackTrace();
            return res;
        }
    }
%>
<%
    String CrsPath = "CRSService";
    String RootUrl = url_CRS() + "/" + CrsPath;
    SoapMiddleware soapMiddleware = new SoapMiddleware();

    X("RootUrl "+ RootUrl);
    
	response.addHeader("Access-Control-Allow-Origin","*");
   	//String sessionid = request.getSession().getId(); 
	//response.setHeader("Set-Cookie", "JSESSIONID=" + sessionid + "; HttpOnly; Secure");
    
    String method = request.getParameter("method");
    String param = request.getParameter("param");
    String err = null;
  
    if(soapMiddleware.isStrNull(method)){
        err = "method is null";
    }

    else{
        // trim whitespaces
        method = method.trim();
        param = param.trim();

        String endpointUrl = RootUrl;
        JSONObject jsonParam = soapMiddleware.getJsonCallForCrs(method, param);
        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + CrsPath;
        String myNamespace = "wzs";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);

        responseSoap = fixByteVal(responseSoap, "<HafRfingIdx>");
        responseSoap = fixByteVal(responseSoap, "<HafLfingIdx>");
        %>
        <%= responseSoap %>
        <%
    }

    if(!soapMiddleware.isStrNull(err)){
      %><%= err %><%
    }
%>
