<%@ page import="java.io.*"%>
<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="org.json.simple.parser.JSONParser" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@ page import="java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@page import="java.net.HttpURLConnection" %>
<%@page import="java.net.URL" %>

<%! public Boolean PRINT_INFO = false; %>
<%! public String VERSION = "v2"; %>
<%! public String SOAP_MIDDLEWARE = "soap-middleware"; %>
<%! public String EJPN_CONFIG_FILENAME = "ejpn-config.json" ; %>
<%! public Boolean IS_PROD = true; %>
<%-- <%! public String REQUEST_URL = ""; %> --%>
<%-- <%! public String BASE_URL = ""; %> --%>
<%! public String HOST_URL = ""; %>
<%! public String REQUEST_PATH = ""; %>
<%! public String FORM_URL_ENCODED = "application/x-www-form-urlencoded"; %>

<%
    REQUEST_PATH = request.getRealPath(request.getServletPath());

    //REQUEST_URL = request.getRequestURL().toString();
	//BASE_URL = REQUEST_URL.substring(0, REQUEST_URL.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
	//HOST_URL = REQUEST_URL.substring(0, REQUEST_URL.length() - request.getRequestURI().length());
	
	HOST_URL = "http://localhost:" + request.getServerPort()  + "/" ;

    //IS_PROD = !(request.getRequestURL().toString().contains("localhost"));
	
%>

<%!
	// ############################################## 
	// GLOBAL GETTER 
	final String url_SoapMiddleware(){
		//String rawPath = REQUEST_URL;
        //String[] arr = rawPath.split(SOAP_MIDDLEWARE);
        //String toRet = arr[0] + SOAP_MIDDLEWARE;
		String toRet = HOST_URL + SOAP_MIDDLEWARE;
        return toRet;
    }

    final String url_WAS(){
		String Root = "http://WASIP:" + config_WAS_PORT() + "/";

        //String Root = "";
        //if(IS_PROD){
        //    Root = "http://WASIP:" + config_WAS_PORT() + "/";
        //}else{
        //    Root = "http://10.23.196.65:9080/";
        //}

        return Root;    
    }

	final String url_CRS(){
		String url = "http://" + config_CRS_HOST() + ":" + config_CRS_PORT();
		return url;
	}


	// aidy
	final String url_NewMDSCancel(){
		String url = HOST_URL + "ejpn-mykid/mykid/cancel/inq";

		//String url = "";
		//if(IS_PROD){
       	//	url = "http://MDSIP:" + config_MDS_PORT() + "/api/eJPNmyKidCancel";
		//} else {
		//	url = "http://localhost:" + config_MDS_PORT() +  "/api/eJPNmyKidCancel";
		//}

		return url;   
    }

	// aidy
	final String url_NewMDS(){
		String url = HOST_URL + "ejpn-mykid/mykid/express/inq";
		
		//String url = "";
		//if(IS_PROD){
       	//	url = "http://MDSIP:" + config_MDS_PORT() + "/api/eJPNmyKidExpress";
		//} else {
		//	url = "http://localhost:" + config_MDS_PORT() +  "/api/eJPNmyKidExpress";
		//}

		return url;   
    }

	// teoh
	final String url_MDSCancel(){
		String url = "http://MDSIP:" + config_MDS_PORT() + "/api/eJPNmyKidCancel";

		//String url = "";
		//if(IS_PROD){
       	//	url = "http://MDSIP:" + config_MDS_PORT() + "/api/eJPNmyKidCancel";
		//} else {
		//	url = "http://localhost:" + config_MDS_PORT() +  "/api/eJPNmyKidCancel";
		//}

		return url;   
    }

	// teoh
	final String url_MDS(){
		String url = "http://MDSIP:" + config_MDS_PORT() + "/api/eJPNmyKidExpress";
		
		//String url = "";
		//if(IS_PROD){
       	//	url = "http://MDSIP:" + config_MDS_PORT() + "/api/eJPNmyKidExpress";
		//} else {
		//	url = "http://localhost:" + config_MDS_PORT() +  "/api/eJPNmyKidExpress";
		//}

		return url;   
    }
	
	final String path_SoapMiddleware(){
		String[] arr = REQUEST_PATH.split(SOAP_MIDDLEWARE);
		return arr[0] + SOAP_MIDDLEWARE;
	}

	final String path_ServerRoot(){
		String[] arr = REQUEST_PATH.split(SOAP_MIDDLEWARE);
		return arr[0];
	}

	final String path_EjpnConfig(){
		return path_ServerRoot() + EJPN_CONFIG_FILENAME;
	}

	final String time_currentString(String pattern){
		String patternTimestamp = pattern;
		SimpleDateFormat time_formatter_timestamp = new SimpleDateFormat(patternTimestamp);
		String curTimeStamp = time_formatter_timestamp.format(System.currentTimeMillis());
		return curTimeStamp;
	}

	// ############################################## 
	// GLOBAL CONFIG GETTER 
	final String config_CRS_HOST(){
		return config_getByKey("CRS_HOST");	
	}
	final String config_CRS_PORT(){
		return config_getByKey("CRS_PORT");	
	}

	final String config_MDS_PORT(){
		return config_getByKey("MDS_PORT");	
	}
	
	final String config_WAS_PORT(){
		return config_getByKey("WAS_PORT");	
	}

	final String config_getByKey(String key){
		String raw = readEjpnConfig();
		JSONObject obj = parseJson(raw);
		return (String) obj.get(key);
	}

	// ############################################## 
	// GLOBAL HELPER 
	final String post(String url, String urlParam, String contentType){
		String result = null;
		String error = null;

		try{

			//String host = "10.23.191.124";
			//String port = "8080";
			//String action = "INQ_USER";      
			//String userId = "EJPN005";
			//String prm = "{\"UserId\": \""+ userId +"\"}";        

			if(url == null || urlParam == null){
				throw new Exception("Required parameter is null (host, port, action, param)");
			}             

			String postParam = urlParam;

			//System.out.println("host " + host);
			//System.out.println("action " + action);
			//System.out.println("param " + param);
			//System.out.println("url " + url);

			// 2. create connection
			URL obj = new URL(url);        
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", contentType);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			conn.setDoOutput(true);

			// 3. add parameter to body
			OutputStream os = conn.getOutputStream();
			byte[] input = postParam.getBytes("utf-8");
			os.write(input, 0, input.length); 
			os.close(); 

			// 4. read response
			BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
			java.io.ByteArrayOutputStream buf = new java.io.ByteArrayOutputStream();
			int result2 = bis.read();
			while(result2 != -1) {
				buf.write((byte) result2);
				result2 = bis.read();
			}
			result = buf.toString();
			return result;

		}catch(Exception err){
			error = err.toString();
			return error;
		}
	}
	final JSONObject parseJson(String stringToParse){
		try{
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(stringToParse);
			return json;
		}catch(Exception err){
			err.printStackTrace();
			return null;
		}
	}
	final void X(Object x){
        System.out.println(x.toString()); 
    }
    final void X(String label, Object x){
        System.out.println(label + " **********\n" + x.toString() + "\n"); 
    }
	final String readEjpnConfig(){
		return readFile(path_EjpnConfig());
	}
    final String readFile(String filePath){
		String toRet = "";
		try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
		
			while (line != null) {
				sb.append(line);
				//sb.append(System.lineSeparator());
				sb.append("\n");
				line = br.readLine();
			}
			toRet = sb.toString();
            br.close();
		} catch(Exception e){
		    e.printStackTrace();
		} 
		return toRet;
    }
	final String createFile(String filePath, String content){
        try {
            if(content == null){
                return "Failed to create "+ filePath + "<br><br>Content Is NULL";
            }            
            PrintWriter pw = new PrintWriter(new FileOutputStream(filePath));
            pw.println(content);
            pw.close();
            return "Successfully created "+ filePath + "<br><br>";
        } catch(IOException e) {
            return "Failed to create "+ filePath + "<br><br>" + e.getMessage();
        }
    }

	final String appendFile(String filePath, String content){
		try {
			File file = new File(filePath);
			FileWriter fr = new FileWriter(file, true);
			BufferedWriter br = new BufferedWriter(fr);
			PrintWriter pr = new PrintWriter(br);
			pr.println(content);
			pr.close();
			br.close();
			fr.close();
            return "Successfully append to "+ filePath + "<br><br>";
		 } catch(IOException e) {
            return "Failed to append "+ filePath + "<br><br>" + e.getMessage();
        }
	}

%>
<%

if(PRINT_INFO){ 
	X("###############################################################");
	X("### GLOBAL ####################################################");


	X("VERSION",VERSION);
	X("IS_PROD",IS_PROD);
	//X("REQUEST_URL",REQUEST_URL);
	X("REQUEST_PATH",REQUEST_PATH);
	//X("BASE_URL",BASE_URL);
	X("HOST_URL",HOST_URL);
	X("url_SoapMiddleware()",url_SoapMiddleware());
	X("url_WAS()",url_WAS());
	X("path_ServerRoot()",path_ServerRoot());
	X("path_SoapMiddleware()",path_SoapMiddleware());
	X("config_WAS_PORT()",config_WAS_PORT());
	X("readEjpnConfig()",readEjpnConfig());
	
	X("### GLOBAL ####################################################");
	X("###############################################################");
}

%>

