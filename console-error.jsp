<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%! public Boolean IS_PROD = false; %>
<%! public String BREAK_LINE = ""; %>
<%
    BREAK_LINE = System.getProperty("line.separator");
    IS_PROD = !(request.getRequestURL().toString().contains("localhost"));
%>

<%!
    final void X(String x){
        if(!IS_PROD){
            System.out.println("[X] " + x); 
        }
    }

    final String getCurrentTime(){
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat time_formatter = new SimpleDateFormat(pattern);
        String toRet = time_formatter.format(System.currentTimeMillis());
        return toRet;
    }

    final String getTodayDate(){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat time_formatter = new SimpleDateFormat(pattern);
        String toRet = time_formatter.format(System.currentTimeMillis());
        return toRet;
    }

    final String appendLog(String fileName, String content){
        if(fileName == null || fileName.equals("")){
            return "File Name Empty";
        }

        if(content == null || content.equals("")){
            return "Content Empty";
        }

        // get log file path
        //C:\JBOSS\jboss-5.1.0.GA\bin
        String BIN_PATH = new File("").getAbsolutePath();
        String filePath = BIN_PATH.replace("bin", "");
        //filePath += "server\\default\\deploy\\soap-middleware.war\\log\\" + fileName + ".log";
        //System.out.println("filePath" +filePath);
        
		filePath += "\\webapps\\soap-middleware\\log\\" + fileName + ".log";
        
        try{
            Writer fileWriter = new FileWriter(filePath, true);
            fileWriter.write(content);
            fileWriter.write(System.getProperty("line.separator"));
            fileWriter.close();
            return "Successfully created "+ filePath + "<br><br>";
        }catch(IOException e){
            return "Failed to create "+ filePath + "<br><br>" + e.getMessage();
        }
    }
%>
<%
// #############################################################################
// START MAIN PROGRAM
    response.addHeader("Access-Control-Allow-Origin","*");

    String meta = request.getParameter("meta");
    String err = request.getParameter("err");

	Boolean skip = false;
	if(err.contains("generateBreacrumbsFromStore")){
		skip = true;
	}

	if(!skip){
		String fileName = "console-error_" + getTodayDate();
		String content = "";
		content += ".";
		content += BREAK_LINE;
		content += "[time] " + getCurrentTime();
		content += BREAK_LINE;
		content += "[meta] " + meta;
		content += BREAK_LINE;
		content += "[err] " + err;
		String res = appendLog(fileName, content);
	}
    
%>