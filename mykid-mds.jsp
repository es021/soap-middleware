<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@ include file="_global.jsp" %>

<%
	response.addHeader("Access-Control-Allow-Origin","*");
    String hscno = request.getParameter("hscno");
    //String aplno = request.getParameter("aplno");
    String rec_ind = request.getParameter("rec_ind");
    
    // 1. get request url
    String url = "";
    if(rec_ind != null && rec_ind.equals("2")){
        url = url_NewMDSCancel();
    }else{
        url = url_NewMDS();
    }
    url += "/" + hscno;

    X("==============================================================");
    X("mykid-mds.jsp");
    X("url", url);
    X("rec_ind", rec_ind);

    String recv = "";
    String recvbuff = "";
    URL jsonpage = new URL(url);
    URLConnection urlcon = jsonpage.openConnection();
    BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));

    while ((recv = buffread.readLine()) != null){
        recvbuff += recv;
    }

    buffread.close();
%>
<%= recvbuff %>
