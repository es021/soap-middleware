<%@ page import="java.io.*"%>
<%@ page import="com.jpn.soap.SoapMiddleware" %>
<%@ page import="org.json.simple.JSONObject" %>

<%!
    // HARDCODE PATH -- DONT USE THIS
    
    // http://10.23.196.65:9080/soap-middleware/create-ref.jsp?ref-code=005
    // http://10.23.196.65:9080/soap-middleware/ref
    // http://10.23.196.65:9080/soap-middleware/ref/ref-005.xml
    
    final String createFile(String AssetPath, String fileName, String content){
        String filePath = AssetPath + "/" + fileName + ".xml";
        try {   
            PrintWriter pw = new PrintWriter(new FileOutputStream(filePath));
            pw.println(content);
            pw.close();
            return "Successfully created "+ filePath + "<br><br>";
        } catch(IOException e) {
            //System.out.println(e.getMessage());
            return "Failed to create "+ filePath + "<br><br>" + e.getMessage();
        }
    }

    final String getRefData(String refCode){
        String RootUrl = "http://10.23.196.65:9080/";
        String webService = "SREF/SREF_ENTITY";
        String method = "SrefEntity";
        String endpointUrl = RootUrl + webService;

        SoapMiddleware soapMiddleware = new SoapMiddleware();

        String jsonString = "{\"InRefEntityType\":{\"Type\":\"" + refCode + "\"}}";
        JSONObject jsonParam = soapMiddleware.getJsonCall(method, jsonString);

        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + method + "/";
        String myNamespace = "wzs";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);
        return responseSoap;
    }

%>

<%

    response.addHeader("Access-Control-Allow-Origin","*");

    String refCode = request.getParameter("ref-code");
    String mesFile = "";
    String xmlString = "";
    if(refCode != null){
        xmlString = getRefData(refCode);

        if(xmlString.contains("<row><RefEntity><Desc>") || xmlString.contains("<row><RefEntity><Code>")){
            String AssetPath = "/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/installedApps/eJPN2-devNode01Cell";
            AssetPath += "/soap-middleware.ear/soap-middleware.war/ref";
            String fileName = "ref-" + refCode;
            mesFile = createFile(AssetPath, fileName, xmlString);
        }else{
            mesFile = "Content Empty. File not created.";
        }
    }
%>
<h3>Creating ref file for ref table <%= refCode %></h3>
<h3>ref-code</h3>
<%= refCode %>
<h3>Response From Creating File</h3>
<%= mesFile %>
<h3>Response From Soap</h3>
<%= xmlString %>






