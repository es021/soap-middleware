<%@ page import="java.io.*"%>
<%@ page import="com.jpn.soap.SoapMiddleware" %>
<%@ page import="org.json.simple.JSONObject" %>
<%@ include file="_global.jsp" %>
<%!
    final String getRefData(String refCode){
        String webService = "SREF/SREF_ENTITY";
        String method = "SrefEntity";
        String endpointUrl = url_WAS() + webService;

        SoapMiddleware soapMiddleware = new SoapMiddleware();

        String jsonString = "{\"InRefEntityType\":{\"Type\":\"" + refCode + "\"}}";
        JSONObject jsonParam = soapMiddleware.getJsonCall(method, jsonString);

        String soapEndpointUrl = endpointUrl;
        String soapAction = endpointUrl;
        String myNamespaceURI = "http://tempuri.org/" + method + "/";
        String myNamespace = "wzs";

        // make soap call to was
        String responseSoap = soapMiddleware.callSoapWebService(soapAction, myNamespace, myNamespaceURI, jsonParam);
        return responseSoap;
    }
%>
<%
    response.addHeader("Access-Control-Allow-Origin","*");
    // 1. get ref Code
    String RefCode = request.getParameter("ref-code");

    if(RefCode == null){
        RefCode = request.getParameter("RefCode");
    }
    if(RefCode == null){
        RefCode = request.getParameter("RefCode");
    }

    X("START -->> get-ref-from-db2 -->" + RefCode);

    // 2. ger ref path in server
    String mesFile = "";
    String xmlString = "";
    if(RefCode != null){
        xmlString = getRefData(RefCode);
    }
    X("END -->> get-ref-from-db2 -->" + RefCode);
%>
<%= xmlString %>






