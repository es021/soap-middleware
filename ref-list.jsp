<%@ page import="java.io.*"%>
<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="java.net.*" %>
<%@ include file="_global.jsp" %>

<%! public String GET_REF_FROM_DB2 = "get-ref-from-db2.jsp"; %>
<%! public String ErrRefCodeIsNull = "ErrRefCodeIsNull"; %>
<%! public String ErrRefContentNotValid = "ErrRefContentNotValid"; %>
<%! public String ErrIndexIsNull = "ErrIndexIsNull"; %>
<%! public String ErrRefCodeArrEmpty = "ErrRefCodeArrEmpty"; %>

<%!
    final String getIndex(String url){
        try{
           String curTimeStamp = time_currentString("yyyyMMddHHmmss");
            url += "?v=" + curTimeStamp;
            
            X("[getIndex] " + url);

            String recvbuff = "";
            String recv = "";

            URL jsonpage = new URL(url);
            URLConnection urlcon = jsonpage.openConnection();
            BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));

            while ((recv = buffread.readLine()) != null){
                recvbuff += recv;
            }
                
            buffread.close();
            return recvbuff;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    final String[] getRefCodeArrFromIndex(String index){
        String[] indexArr = index.split("</a>");
        Integer curIndex = 0;
        String[] toRet = new String[500];
        for(Integer i = 0; i < indexArr.length; i++){
            String row = indexArr[i];    
            String[] rowArr = row.split(".xml");
            if(rowArr.length >= 1){
                row = rowArr[0];              
                rowArr = row.split("/ref-");
                if(rowArr.length >= 2){
                    String refCode = rowArr[1];
                    toRet[curIndex] = refCode;
                    curIndex ++;
                }
            }
        }

        return toRet;
    }

    final String[] getRefList(String url){
        String index = getIndex(url);
        if(index == null){
            return null;
        }

        String[] refCodeArr = getRefCodeArrFromIndex(index);
        return refCodeArr;
    }

    final String arrToString(String[] arr){
        String toRet = "";
        for(Integer i = 0; i < arr.length; i++){
            String row = arr[i];    
            if(row == null || row.equals("")){
                continue;
            }
            toRet += "<li>ref-" + row + "</li>";
        }

        return toRet;
    }
%>

<%
    response.addHeader("Access-Control-Allow-Origin","*");
    // create url
    String REF_INDEX = "soap-middleware/ref/index.jsp";
    String local = arrToString(getRefList("http://localhost:8080/" + REF_INDEX));
    String dev = arrToString(getRefList("http://10.23.191.124:8080/" + REF_INDEX));
    String staging = arrToString(getRefList("http://10.23.191.128:8080/" + REF_INDEX));
    String bka = arrToString(getRefList("http://10.23.189.90:80/" + REF_INDEX));
    String bkc = arrToString(getRefList("http://10.23.189.20:80/" + REF_INDEX));

%>
<style>
    .main{
        display:flex;
    }

    .col {
        padding : 0px 10px;
    }
</style>
<hr>
<div class="main">
    <%-- 
    <div class="col">
        <h3>Local - localhost:8080</h3>
        <ol>
            <%= local %>
        </ol>
    </div> 
    --%>

    <div class="col">
        <h3>Dev - 191.124</h3>
        <ol>
            <%= dev %>
        </ol>
    </div>

    <div class="col">
        <h3>Staging - 191.128</h3>
        <ol>
            <%= staging %>
        </ol>
    </div>

    <div class="col">
        <h3>BKA - 189.90</h3>
        <ol>
            <%= bka %>
        </ol>
    </div>

    <div class="col">
        <h3>BKC - 189.20</h3>
        <ol>
            <%= bkc %>
        </ol>
    </div>

</div>
