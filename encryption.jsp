<%@page import="java.net.URLEncoder" %>
<%@page import="java.net.URLDecoder" %>
<%@page import="java.util.Base64" %>
<%@page import="java.security.Key" %>
<%@page import="javax.crypto.Cipher" %>
<%@page import="javax.crypto.spec.SecretKeySpec" %>

<%-- has to be of len 16 --%>
<%! public String SECRET = "EjpnEncryption11"; %>
<%-- <%! public String SECRET = "Bar12345Bar12345"; %> --%>
<%!

    final Cipher initCypher(){
        try 
        {
            Cipher cipher = Cipher.getInstance("AES");
            return cipher;
        }
        catch(Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }

    final SecretKeySpec getSecret(){
        try 
        {
            String key = SECRET; // 128 bit key
            // Rijndael
            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
            return secretKey;
        }
        catch(Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }

    final String encrypt(String toEncrypt){
       try 
        {
            Cipher cipher = initCypher();
            cipher.init(Cipher.ENCRYPT_MODE, getSecret());
            byte[] encrypted = cipher.doFinal(toEncrypt.getBytes());
            String encryptedStr = new String(encrypted);
            System.err.println("encryptedStr "+encryptedStr);
            return encryptedStr;
        }
        catch(Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }

    final String  encode64(String toEncode){
        // Getting MIME encoder  
        Base64.Encoder encoder = Base64.getMimeEncoder();  
        String eStr = encoder.encodeToString(toEncode.getBytes());  
        return eStr;
    }

    final String encodeUrl(String toEncode) throws Exception{
        String encodedUrl = URLEncoder.encode(toEncode, "UTF-8");
        return encodedUrl;
    }

    final String decodeUrl(String toDecode) throws Exception{
        String decodedUrl = URLDecoder.decode(toDecode);
        return decodedUrl;
    }

    final String  decode64(String toDecode){
          
        // Getting MIME decoder  
        Base64.Decoder decoder = Base64.getMimeDecoder();  
        // Decoding MIME encoded toEncode  
        String dStr = new String(decoder.decode(toDecode));  
        return dStr;
    }

    
    final String decrypt(String toDecrypt){
       try 
        {
            Cipher cipher = initCypher();
             // decrypt the toEncrypt
            cipher.init(Cipher.DECRYPT_MODE, getSecret());
            String decrypted = new String(cipher.doFinal(toDecrypt.getBytes()));
            System.err.println("decrypted "+decrypted);
            return decrypted;

        }
        catch(Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }

%>
<%
	response.addHeader("Access-Control-Allow-Origin","*");

    String action = request.getParameter("action");
    String data = request.getParameter("data");
    String res = "";


    System.out.println("-------------------");  
    System.out.println("action "+action );
    System.out.println("data "+data );
// localhost:8080/soap-middleware/encryption.jsp?action=encrypt&data=WanMuhammadAlFateh

    try{
        if(action.equals("encrypt")){   
            res = encrypt(data);

            System.out.println(res);
            res = encode64(res);

            System.out.println(res);
            res = encodeUrl(res);

        } else if(action.equals("decrypt")){
            //res = decodeUrl(data);

            res = decode64(data);
            System.out.println(res);
            res = decrypt(res);
        }
    }catch(Exception e){
        e.printStackTrace();
        res = e.toString();
    }
  
%>

<%= res %>

<%-- <%= encoded.length() %><br>
encrypted : 
<%= encrypted %>
<br>
encoded : 
<%= encoded %>
<br>
decoded : 
<%= decoded %>
<br>
decrypted : 
<%= decrypted %>
 --%>
