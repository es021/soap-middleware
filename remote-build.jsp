<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.io.IOUtils" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.net.HttpURLConnection" %>
<%@page import="java.net.URL" %>

<%! 
    final String post(String action, String param, String host) {
        try{
            System.out.println("host param " + host);

            // 1. get request url
            if(host == null || host.equals("")){
                host = "localhost";    
            }
            String url = "http://" + host + ":3000/" + action;
            System.out.println("url " + url);

            // 2. create connection
            URL obj = new URL(url);        
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setDoOutput(true);

            // 3. add parameter to body
            OutputStream os = conn.getOutputStream();
            byte[] input = param.getBytes("utf-8");
            os.write(input, 0, input.length); 
            os.close(); 
        
            // 4. read response
            String result;
            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
            java.io.ByteArrayOutputStream buf = new java.io.ByteArrayOutputStream();
            int result2 = bis.read();
            while(result2 != -1) {
                buf.write((byte) result2);
                result2 = bis.read();
            }
            result = buf.toString();

            return result;
        }catch(Exception err){
            return err.toString();
        }
      
    }
%>
<%
   String __HOST__ = request.getRequestURL().toString();

   Boolean IS_PROD = !(__HOST__.contains("localhost"));
   if(__HOST__.contains("10.23.171.71")){
        IS_PROD = false;
   }
   
   String tempPath = "";
   String filePath = "";
   String webappPath = "";
   if(__HOST__.contains("192.168.0.240")){
        tempPath = "c:\\temp";
        webappPath = "c:/Tomcat-8.5/webapps";
   }
   else if(IS_PROD){
        tempPath = "d:\\temp";
        webappPath = "d:/Tomcat-8.5/webapps";
   }else{
        tempPath = "c:\\JBOSS\\temp";
        webappPath = "c:/JBOSS/Tomcat-8.5/webapps";
   }
   filePath = webappPath + "/docs/";

   String action = request.getParameter("action");
   String userId = request.getParameter("userId");
   String host = request.getParameter("host");
   String port = request.getParameter("port");
   String project = request.getParameter("project");

   String targetPort = port == null ? "8080" : port;

    if(action.equals("build") || action.equals("deploy")){

        // save uploaded file first
        File file ;
        int maxFileSize = 100000 * 1024;
        int maxMemSize = 100000 * 1024;

        String pattern = "yyyyMMddHHmmss";
        SimpleDateFormat time_formatter = new SimpleDateFormat(pattern, Locale.ENGLISH);
        String current_time_str = time_formatter.format(System.currentTimeMillis());
        
        String contentType = request.getContentType();
        if ((contentType.indexOf("multipart/form-data") >= 0)) {

            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(maxMemSize);
            factory.setRepository(new File(tempPath));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax( maxFileSize );
            try{ 
                List fileItems = upload.parseRequest(request);
                Iterator i = fileItems.iterator();
                while (i.hasNext()) 
                {
                    FileItem fi = (FileItem)i.next();
                    if ( !fi.isFormField () )  {
                        String fieldName = fi.getFieldName();
                        String fileName = fi.getName();
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();
                        String createdFilePath = filePath + current_time_str + "_" + fileName;
                        file = new File(createdFilePath) ;
                        fi.write(file) ;
                        
                        // staging
                        if(host != null && !host.equals("") && !host.equals("10.23.191.124")){
                            //Thread.sleep(1000);
                            String command = "curl -F \"file=@" + createdFilePath + "\" \"http://" + host + ":" + targetPort + "/soap-middleware/remote-build.jsp?action=deploy&userId=" + userId + "&project=" + project + "\"";
                            System.out.println(command);
                            System.out.println(command);
                            System.out.println(command);
                            System.out.println(command);
                            Process p = Runtime.getRuntime().exec(command);
                            p.waitFor();
                            String stdout = IOUtils.toString(p.getInputStream());
                            String stderr = IOUtils.toString(p.getErrorStream());
                            if(!stdout.equals("")){
                                out.println(stdout);
                            } else if(!stderr.equals("")){
                                out.println(stderr);
                            }                     
                        } else{
                            // 1. Build Or Deploy - send post to node api
                            String param = "{\"filePath\": \""+ createdFilePath +"\",\"userId\": \""+ userId +"\",\"webappPath\": \""+ webappPath +"\",\"project\": \""+ project +"\"}";
                            String toPrint = post(action, param, host);
                            out.println(toPrint);
                        }
                    }
                }
            }catch(Exception ex) {
                System.out.println(ex);
            }
        }else{
            out.println("No file uploaded"); 
        }
    }else{
        String param = "{}";
        String toPrint = post(action, param, host);
        System.out.println(toPrint);
        out.println(toPrint);
    }

    



%>