
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.net.*" %>

<%! public String JPN_TRANSFORMASI = "jpn-transformasi"; %>
<%! public String SOAP_MIDDLEWARE = "soap-middleware"; %>
<%! public Boolean IS_PROD = false; %>
<%! public String CUSTOM_HOST = null; %>
<%
    IS_PROD = !(request.getRequestURL().toString().contains("localhost"));
    CUSTOM_HOST = request.getParameter("host");
%>

<%!
    /**
        USAGE : 
        http://10.23.191.124:8080/soap-middleware/get-app-info.jsp?host=10.23.191.124
    **/

    final void X(String x){
        System.out.println(x); 
    }

    final void X(String label, String content){
        System.out.println("[" + label + "]\n" + content + "\n"); 
    }

  
    final String getWebappOrDeployPath(String currentPath){
        String[] arr = currentPath.split(SOAP_MIDDLEWARE);
        return arr[0];
    }

    final String getAppUrl(String reqUrl){
        String[] arr = reqUrl.split(SOAP_MIDDLEWARE);
        String toRet = arr[0] + JPN_TRANSFORMASI;
        if(CUSTOM_HOST != null){
            toRet = "http://" + CUSTOM_HOST + ":8080/" + JPN_TRANSFORMASI;
        }
        return toRet;
    }


    final String getAppIndexContent(){
        return "";
    }

    final String _GET (String url){
        X("get url", url);
        try{
            // get current timestamp
            String patternTimestamp = "yyyyMMddHHmmss";
            SimpleDateFormat time_formatter_timestamp = new SimpleDateFormat(patternTimestamp);
            String curTimeStamp = time_formatter_timestamp.format(System.currentTimeMillis());
            url = url + "?v=" + curTimeStamp;


            String recvbuff = "";
            String recv = "";
            
            URL jsonpage = new URL(url);
            URLConnection urlcon = jsonpage.openConnection();
            BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));

            while ((recv = buffread.readLine()) != null){
                recvbuff += recv;
            }
                
            buffread.close();
            return recvbuff;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    } 

    final String getJsPath(String indexContent){
        String[] arr = indexContent.split("build/js/app.");
        String temp = arr[1];
        arr = temp.split(".js");
        String jsVer = arr[0];
        X("jsVer",jsVer);
        return "build/js/app." + jsVer + ".js";
    }

    final String getAppBuildPath(String jsContent){
        String[] arr = jsContent.split("BUILD_PATH");
        String temp = arr[1];
        arr = temp.split("build.js");
        temp = arr[0];
        String buildPath = temp.substring(3 ,temp.length() - 1 );
        return buildPath + "build.js";
    }

    final String getAppVer(String jsContent){
        String[] arr = jsContent.split("init buildTime");
        String temp = arr[0];
        String ver = temp.substring(temp.length() - 24 ,temp.length() - 14 );
        ver = getDateTimeFromUnix(ver);
        return ver;
    }

    final String getDateTimeFromUnix(String unix){
        long unix_seconds = Long.parseLong(unix);
        //convert seconds to milliseconds
        Date date = new Date(unix_seconds*1000L); 
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        //jdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);
        return java_date;
    }
%>
<%
    String APP_URL = getAppUrl(request.getRequestURL().toString());
    X("APP_URL", APP_URL);

    String appIndexUrl = APP_URL + "/index.html";
    X("appIndexUrl", appIndexUrl);

    String indexContent = _GET(appIndexUrl);
    X("indexContent", indexContent);

    String jsUrl = APP_URL + "/" + getJsPath(indexContent);
    X("jsUrl", jsUrl);

    String jsContent = _GET(jsUrl);

    String appVer = getAppVer(jsContent);
    X("appVer", appVer);

    String buildPath = getAppBuildPath(jsContent);
    X("buildPath", buildPath);
%> 
{
    "version" : "<%= appVer %>",
    "buildPath" : "<%= buildPath %>"
}
