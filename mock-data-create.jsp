<%@ page import="java.io.*"%>
<%@ page import="org.json.simple.JSONObject" %>

<%!
    // guna kat JBOSS
    final String createFile(String AssetPath, String fileName, String content){
        String filePath = AssetPath + "/" + fileName + ".json";
        try {   
            PrintWriter pw = new PrintWriter(new FileOutputStream(filePath));
            pw.println(content);
            pw.close();
            return "Successfully created "+ filePath + "<br><br>";
        } catch(IOException e) {
            System.out.println(e.getMessage());
            return "Failed to create "+ filePath + "<br><br>" + e.getMessage();
        }
    }
%>

<%
    String PARAM_FILENAME = "filename";
    String PARAM_CONTENT = "content";

    response.addHeader("Access-Control-Allow-Origin","*");

    String fileName = request.getParameter(PARAM_FILENAME);
    String content = request.getParameter(PARAM_CONTENT);
    String mesFile = "";
    String contentToWrite = "";
    String requestPath = request.getRealPath(request.getServletPath());
    String thisFile = "mock-data-create.jsp";
    String AssetPath = requestPath.replace(thisFile,"mock-data");

    if(fileName != null && content != null){
        contentToWrite = content;
        mesFile = createFile(AssetPath, fileName, content);
    }

%>
<h3>Creating mock data file for filename <%= fileName %></h3>
<h3>Response From Creating File</h3>
<%= mesFile %>

<h3>FileName</h3>
<%= fileName %>

<h3>Content</h3>
<%= content %>

<h3>requestPath</h3>
<%= requestPath %>

<h3>AssetPath</h3>
<%= AssetPath %>







