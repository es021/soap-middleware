<%@ page import="java.io.*"%>
<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="java.net.*" %>
<%@ include file="_global.jsp" %>


<%! public String GET_REF_FROM_DB2 = "get-ref-from-db2.jsp"; %>
<%! public String ErrRefCodeIsNull = "ErrRefCodeIsNull"; %>
<%! public String ErrRefContentNotValid = "ErrRefContentNotValid"; %>
<%! public String ErrIndexIsNull = "ErrIndexIsNull"; %>
<%! public String ErrRefCodeArrEmpty = "ErrRefCodeArrEmpty"; %>

<%!
    /** 
    @author Wan Zulsarhan
    @Description - Use In JBOSS server
        0. Copy ref.xml from WAS -> JBOSS
        1. check kat log [ref-last-update.log] nak update ke tak
            a. kalau ada param 'forceUpdate=1' akan update trus tanpa check log file
        2. download all ref.xml from was to jboss
            a. kalau ada param  'refCode=xxx' akan update ref code tu je
        3. update file log kalau berjaya update semua ref file
            a. kalau ada param 'refCode=xxx' tak akan update file log
    @Example
        1. http://10.23.191.124:8080/soap-middleware/update-server-ref.jsp?updateWas=1
            a. DB2->JBOSS (letak dalam on load program front end)
            b. dependency asset kat JBOSS
                i. soap-middleware/log/ref-last-update.log
                ii. soap-middleware/get-ref-from-db2.jsp
        2. http://10.23.191.124:8080/soap-middleware/update-server-ref.jsp 
            a. untuk update sume if current date > date from log file
        3. http://10.23.191.124:8080/soap-middleware/update-server-ref.jsp/?forceUpdate=1
            a. untuk update sume tak kira date from log file
        4. http://10.23.191.124:8080/soap-middleware/update-server-ref.jsp/?forceUpdate=1&refCode=007
            a. untuk update ref file 007 je
    **/
  
    final String getRefFromWas(String refCode, Boolean isIndex){
        try{
           String curTimeStamp = time_currentString("yyyyMMddHHmmss");

            // create url
            String Root = url_SoapMiddleware();   
            String url = "";
            if(isIndex){
                url = Root + "/ref/index.jsp" + "?v=" + curTimeStamp;
            } else{
                String JspRoot = Root + "/" + GET_REF_FROM_DB2 ;
                url =  JspRoot + "?ref-code=" + refCode + "&v=" + curTimeStamp;
            }
            
            X("[getRefFromWas] " + url);

            String recvbuff = "";
            String recv = "";

            URL jsonpage = new URL(url);
            URLConnection urlcon = jsonpage.openConnection();
            BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));

            while ((recv = buffread.readLine()) != null){
                recvbuff += recv;
            }
                
            buffread.close();
            return recvbuff;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    final String[] getRefCodeArrFromIndex(String index){
        String[] indexArr = index.split("</a>");
        Integer curIndex = 0;
        String[] toRet = new String[500];
        for(Integer i = 0; i < indexArr.length; i++){
            // <a href="http://10.23.196.65:9080/soap-middleware/ref/ref-103.xml">ref-103.xml - 08/15/2018 15:19:54
            String row = indexArr[i];    
            
            // <a href="http://10.23.196.65:9080/soap-middleware/ref/ref-103
            String[] rowArr = row.split(".xml");

            if(rowArr.length >= 1){
                row = rowArr[0];              
                rowArr = row.split("/ref-");

                if(rowArr.length >= 2){
                    String refCode = rowArr[1];
                    //System.out.println(refCode);
                    toRet[curIndex] = refCode;
                    curIndex ++;
                }
            }
        }

        return toRet;
    }

    final String createNewRefFile(String refPath, String refCode){
        if(refCode != null){
            String contentRef = getRefFromWas(refCode, false);

            if(contentRef.contains("<row><RefEntity><Desc>") || contentRef.contains("<row><RefEntity><Code>")){
                String refFileName = refPath + "ref-" + refCode + ".xml";
                String toRet = createFile(refFileName, contentRef);
                toRet  += "<hr>" + contentRef;
                return toRet;
            } else{
                return ErrRefContentNotValid;
            }

           
        }else{
            return ErrRefCodeIsNull;
        }
    }

    final String doUpdateAll(String refPath){
        String index = getRefFromWas("", true);
        if(index == null){
            return ErrIndexIsNull;
        }

        String[] refCodeArr = getRefCodeArrFromIndex(index);

        if(refCodeArr.length <= 0){
            return ErrRefCodeArrEmpty;
        }
        for(Integer i = 0; i < refCodeArr.length; i++){
            String refCode = refCodeArr[i];
            if(refCode == null){
                continue;
            }
            String res = createNewRefFile(refPath, refCode);
            if(res.contains(ErrRefCodeIsNull)){
                return res;
            }
        }
        return "";
    }

%>

<%
    response.addHeader("Access-Control-Allow-Origin","*");
    String LOG_FILE = "ref-last-update.log";
    String THIS_FILE = "update-server-ref.jsp";
    
    // get current date
    String curDate = time_currentString("yyyyMMdd");


    // get log file path
    String requestPath = request.getRealPath(request.getServletPath());
    String middlewarePath = requestPath.replace(THIS_FILE, "");
    String logPath = middlewarePath + "log/" + LOG_FILE;
    String refPath = middlewarePath + "ref/";

    String mesFile = "";
    String errorUpdateAll = "";
    Boolean doUpdate = false;
    String fileDate = "";

    // force update without checking log file
    String forceUpdate = request.getParameter("forceUpdate");

    // to update only one refCode
    String refCode = request.getParameter("refCode");

    if(forceUpdate != null && forceUpdate.equals("1")){
        doUpdate = true;

    } else {
        
        // read current date from file
        BufferedReader reader = new BufferedReader(new FileReader(logPath));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null){
            sb.append(line);
        }
        fileDate = sb.toString().trim();

        // compare current date with date in log file
        Integer fileDateInt = 0;
        Integer curDateInt = 0;
        try{
            fileDateInt = Integer.parseInt(fileDate);
            curDateInt = Integer.parseInt(curDate);
        }catch(Exception e){
            e.printStackTrace();
        }

        if(curDateInt > fileDateInt){
            // update log file to current date
            doUpdate = true;
        }
    }

    String ref = "";
    if(doUpdate){
        if(refCode != null){
            // for update single
            mesFile = createNewRefFile(refPath, refCode);
        }else{
            // for update all
            String mesRes = doUpdateAll(refPath);
            if(mesRes.equals("")){
                // log ni akan update kalau berjaya update semua ref
                mesFile = createFile(logPath, curDate);
            }else{
                errorUpdateAll = mesRes;
            }
        } 
    }
%>

curDate : <%= curDate %><br>
refPath : <%= refPath %><br>
logPath : <%= logPath %><br>
refCode : <%= refCode %><br>
forceUpdate : <%= forceUpdate %><br>
fileDate : <%= fileDate %><br>
doUpdate : <%= doUpdate %><br>
errorUpdateAll : <%= errorUpdateAll %><br>
mesFile : <%= mesFile %><br>
<hr>
<%= ref %>




