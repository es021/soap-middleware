<%@page import="java.io.*" %>
<%@page import="java.net.HttpURLConnection" %>
<%@page import="java.net.URL" %>
<%@ include file="_global.jsp" %>

<%
	response.addHeader("Access-Control-Allow-Origin","*");
    //Boolean IS_PROD = !(request.getRequestURL().toString().contains("localhost"));
    String hscno = request.getParameter("hscno");
    String aplno = request.getParameter("aplno");
    String rec_ind = request.getParameter("rec_ind");
    
    // 1. get request url
    String url = "";
    if(rec_ind != null && rec_ind.equals("2")){
        url = url_MDSCancel();
    }else{
        url = url_MDS();
    }

    X("==============================================================");
    X("mykid-mds.jsp");
    X("url", url);
    X("hscno", hscno);
    X("aplno", aplno);
    X("rec_ind", rec_ind);

    // 2. create connection
    URL obj = new URL(url);        
    HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
    conn.setRequestMethod("POST");
    conn.setRequestProperty("Content-Type", "application/json");
    conn.setRequestProperty("User-Agent", "Mozilla/5.0");
    conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    conn.setDoOutput(true);

    // 3. add parameter to body
    String jsonInputString = "{\"hscno\": \""+ hscno +"\", \"aplno\": \""+ aplno +"\"}";
    OutputStream os = conn.getOutputStream();
    byte[] input = jsonInputString.getBytes("utf-8");
    os.write(input, 0, input.length); 
    os.close(); 
   
    // 4. read response
    String result;
    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
    ByteArrayOutputStream buf = new ByteArrayOutputStream();
    int result2 = bis.read();
    while(result2 != -1) {
        buf.write((byte) result2);
        result2 = bis.read();
    }
    result = buf.toString();
%>
<%= result %>
