
<%@ page import="java.io.*"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%
    response.addHeader("Access-Control-Allow-Origin","*");

    //System.out.println(request.getRequestURL());
    File jsp = new File(request.getRealPath(request.getServletPath()));
    File dir = jsp.getParentFile();
    File[] list = dir.listFiles();
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    %>
    <h3>Ref Files</h3>
    <%
    for (File f : list) {
        String filePath = f.toString();
        String[] arr = filePath.split("\\\\");
        if(arr.length <= 1){
            arr = filePath.split("/");
        }
    	
        String fileName = arr[arr.length -1];
        String link = request.getRequestURL().toString().replace("index.jsp", "") + fileName;
        String lastModified = sdf.format(f.lastModified());
        %>
        <a href="<%= link %>"><%= fileName %> - <%= lastModified %></a>
        <br>
        <%
    }
%> 
